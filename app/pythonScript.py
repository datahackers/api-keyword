from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals
from pymongo import InsertOne, DeleteOne, ReplaceOne
from requests import get 
import sys
import json
import pymongo

idArticle=sys.argv[1]
# print(sys.argv[1])

uri = "mongodb://cwise:AWHwFVsRcHckfuYGikaRfLiMOsUdvopR0LhxN7lllrcOge8PjQoETdrgWpcuCejEY0RREw3Otg5LT0Nr4hd7HQ==@cwise.documents.azure.com:10255/?ssl=true&replicaSet=globaldb"
client = pymongo.MongoClient(uri)

#POPULATE JSON WITH URL AND THE ARTICLE ID
def createStructAzure():
    data = json.load(open('structPhrase.json', 'r'))
    return data

#CREATE DATA TO AZURE PLAN
def createDataForAzure(document):
    data = createStructAzure()
    i=0
    for i in range(7):
        data["documents"][i]["text"]=document["summary"][str(i)]
        i=i+1
    return data

def uploadToAzure(data):
    url = 'https://eastus.api.cognitive.microsoft.com/text/analytics/v2.0/keyPhrases'
    # print(data)
    payload = json.dumps(data, ensure_ascii=False )
    # print(payload)
    headers = {'Ocp-Apim-Subscription-Key':'a711d73c108a498385b36f96e67270fe','Content-Type': 'application/json','Accept':'application/json'}
    r = requests.post(url, data=payload.encode('utf8'), headers=headers)
    return r.text


#GET THE JSON FROM MONGO
db = client.cwise
cursor = db.cwise.find({"idArticle":str(idArticle)})
data=[]
for document in cursor:
    data = createDataForAzure(document)
keywords = uploadToAzure(data)
keywords = json.loads(keywords)
# print(keywords['documents'])
for i in range(7):
    document['keyword'][str(i)]=keywords['documents'][i]['keyPhrases']
requests = [
    ReplaceOne({'idArticle':idArticle},document)
]

#UPLOAD TO COSMODB
db.cwise.bulk_write(requests)

#CALL NEXT API
payload = {'idArticle': idArticle}
r = get('http://52.234.209.185:8003/', params=payload)
