
var express = require('express')
var bodyParser = require('body-parser')
var PythonShell = require('python-shell');
var mongoClient = require("mongodb").MongoClient;
var app = express()

var jsonParser = bodyParser.json()

var urlencodedParser = bodyParser.urlencoded({ extended: true })

app.use(jsonParser)

app.get('/', jsonParser, function (req, res) {

  if (!req.query.idArticle) return res.sendStatus(400, 'Input the article title')

  idArticle = req.query.idArticle
  idArticle = idArticle.toString()

  var options = {
    mode: 'text',
    pythonPath: 'python',
    pythonOptions: ['-u'], // get print results in real-time
    scriptPath: '/',
    args: [idArticle]
  };

  if (req.body) {
    PythonShell.run('pythonScript.py', options, function (err) {
      if (err) throw err;
    });
    return res.send('200', "Working");

  }
  else { res.send(500, 'No file found') };
})

module.exports = app;